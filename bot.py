# -*- coding: utf-8 -*-
import re
import os
import sys
import json
import requests
from random import (
    choice,
    randint
)
from random_content import (
    get_title,
    get_content
)

directory = os.path.dirname(os.path.abspath(__file__))

# in case social network api has different domain than localhost
# change domain here
api_domain = 'http://127.0.0.1:8000'

api_routes = {
    'register': '%s/api/auth/register/' % api_domain,
    'login': '%s/api/auth/login/' % api_domain,
    'profile': '%s/api/auth/profile/' % api_domain,
    'posts': '%s/api/post/posts/' % api_domain,
}


def print_this(string):
    print(string)


class BotActivity(object):

    def signup_user(self, data):
        # signing up user
        reg = requests.post(api_routes['register'], data)
        # validating if registration was successful
        if reg.status_code == 201:
            print_this('Hey ' + data['email'] + '\n' + 'you have successfully registered!\n')
            return True
        print_this(
            'There was an error:'
        )
        print_this(reg.json())
        return False

    def get_token(self, data):
        # login user with provided data and return token
        data = requests.post(api_routes['login'], data)
        return data.json()['token']

    def get_random_post_data(self):
        data = {
            'title': get_title(),
            'description': get_content(),
            'content': get_content()
        }
        return data

    def create_random_post(self, data, max_posts):
        # creating random post with random data from method and have jwt token
        user = {
            'email': data['email'],
            'password': data['password']
        }
        token = self.get_token(user)
        header = {'Authorization': 'JWT %s' % token}

        # get some random number limited by maximum posts per user
        # and create that amount of posts
        random_post_number = randint(0, max_posts)

        while random_post_number != 0:
            requests.post(api_routes['posts'], self.get_random_post_data(), headers=header)
            random_post_number -= 1

    def like_post(self, slug, post, user):
        # perform post like activity
        token = self.get_token(user)
        header = {'Authorization': 'JWT %s' % token}
        requests.put(api_routes['posts'] + slug, post, headers=header)
        print_this(user['email'] + ' has liked ' + post['title'] + ' : )\n')

    def reset_activity(self, created_users):
        # delete all users
        for user in created_users:
            token = self.get_token(user)
            header = {'Authorization': 'JWT %s' % token}
            requests.delete(api_routes['profile'], headers=header)
            print_this('%s has been deleted successfully.\n' % user['email'])
            # all posts made by users have been also deleted


class AutoBot(BotActivity):
    user_list = []
    users_to_create = None
    created_users = []
    posts_data = []
    max_posts_per_user = None
    max_likes_per_user = None
    users_that_can_like = []

    def __init__(self):
        self.read_config()

    def read_config(self):
        # reading config json file
        informer = os.path.join(directory, 'config.json')
        f = open(informer, 'r')
        output = json.loads(f.read())
        f.close()
        # setting data from json file to variables
        self.users_to_create = output['creation']['users_to_sign']
        self.max_posts_per_user = output['creation']['max_posts_per_user']
        self.max_likes_per_user = output['creation']['max_likes_per_user']
        try:
            self.user_list = output['accounts']
        except IndexError:
            print_this(
                'Sorry but i need list of accounts in config file to perform activity.'
            )
            # stopping bot since this will cause error
            sys.exit()
        # handling potential issue if you want to create more users than are already defined
        if len(self.user_list) < self.users_to_create:
            print_this(
                'Sorry but if you want sign up more users\nthan you have to update account list in json file.'
            )
            # stopping bot since this will cause error
            sys.exit()

    def perform_signup(self):
        # signing up assumes that number of users able to sing up
        # is equal or less than length of account associated within config.json file
        for account in self.user_list:
            if self.users_to_create != 0:
                # sign up this user
                signup = self.signup_user(account)
                # if signup was successful either hunter verified email
                # or other signup requirements have been met then proceed
                if signup:
                    # make this account create some random content post, limited by max posts per user
                    self.create_random_post(account, self.max_posts_per_user)
                    # adding few stuff to each user so we can conduct conditions for reqs
                    account['number_of_liked_posts'] = 0
                    account['posts'] = []
                    self.created_users.append(account)
                    # decrease value only if user is created
                    self.users_to_create -= 1

        self.users_that_can_like = self.created_users

        while len(self.users_that_can_like) != 0:
            self.get_posts()

    def get_posts(self):
        self.posts_data = requests.get(api_routes['posts']).json()
        # setting post data for each user, posts that he has made are assigned to him
        # so we can check further which user has most posts
        for i, user in enumerate(self.users_that_can_like):
            # too big iteration
            self.users_that_can_like[i]['posts'] = [
                post for post in self.posts_data
                if post['author']['email'] == user['email']
            ]
        self.determine_next_user()

    def get_maximum(self):
        return max(self.users_that_can_like, key=lambda x: len(x['posts']))

    def determine_next_user(self):
        # get first user with most posts
        biggest = self.get_maximum()
        # list of posts that can be liked
        # append post from user posts which have atleast one post with 0 likes
        user_posts = [
            user for user in self.created_users
            for post in user['posts']
            if len(post['likes']) == 0
        ]
        # remove user details and append only post data
        # if post author is not current user that is performing like activity
        posts_to_like = [
            post for item in user_posts
            for post in item['posts']
            if post['author']['email'] != biggest['email']
        ]

        while biggest['number_of_liked_posts'] < self.max_likes_per_user:
            # get some random post to like
            post = [choice(posts_to_like) for e in range(len(posts_to_like))]
            # handling no post in post list
            # printing info and exiting due to handling error
            try:
                post[0]['like'] = True
                slug = post[0]['slug'] + '/'
                user = {
                    'email': biggest['email'],
                    'password': biggest['password']
                }
                self.like_post(slug, post[0], user)

                # limit like to one post per user
                for i, obj in enumerate(posts_to_like):
                    if obj['id'] == post[0]['id']:
                        posts_to_like.pop(i)

                biggest['number_of_liked_posts'] += 1
            except IndexError:
                print_this(
                    '- Bot has finished due to requirements...'
                )
                sys.exit()

        # remove users that have finished their liking process
        for i, user in enumerate(self.users_that_can_like):
            if user['email'] == biggest['email']:
                self.users_that_can_like.pop(i)

        print_this(biggest['email'] + ' has finished liking :) \n')

    def start(self):
        self.perform_signup()

    def reset(self):
        try:
            self.reset_activity(self.user_list)
        except KeyError:
            print_this(
                'You are trying to reset activity without performing activity first...'
            )


if __name__ == '__main__':
    bot = AutoBot()
    if len(sys.argv) == 1 or not re.search(r'start|reset', sys.argv[1]):
        print_this(
            '- To start bot execute script with start argument\nExample: python bot.py start\n'
        )
        print_this(
            '- To reset past activities execute script with reset argument\nExample: python bot.py reset'
        )
    elif len(sys.argv) > 1 and re.search(r'start', sys.argv[1]):
        bot.start()
    elif len(sys.argv) > 1 and re.search(r'reset', sys.argv[1]):
        bot.reset()
