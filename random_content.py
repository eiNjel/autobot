# -*- coding: utf-8 -*-
from random import (
    choice,
    randint
)
from string import (
    ascii_letters,
    digits
)


def get_content():
    # some random content generator method would be way too empty without this
    # will return few words depending on randomness
    # not pretty content but serves purpose for now
    string = ''
    random_string_length = randint(3, 10)
    random_word_length = randint(5, 25)

    while random_string_length > 0:
        string += ''.join(choice(ascii_letters + digits) for e in range(random_word_length))
        string += ' '
        random_string_length -= 1
    return string


def get_title():
    # similar method as content but returns one random word
    random_title_length = randint(3, 10)
    return ''.join(choice(ascii_letters + digits) for e in range(random_title_length))
